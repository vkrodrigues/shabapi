﻿using shab.models.Enum;
using shab.models.Models;
using shab.repository.Interface;
using shab.utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace shab.repository.Implementation
{
    public class ParticipanteRepository : IParticipanteRepository
    {
        private readonly List<Participante> Participantes = new();

        private const int IDADEMINIMA = 16;
        private const decimal RENDAMINIMA = 1045;
        private const decimal RENDAMAXIMA = 5225;

        public ParticipanteRepository() => CapturarParticipantes();

        public void CapturarParticipantes()
        {
            StreamReader arquivo = Helper.LerArquivo("shab.repository\\Arquivos", "lista_pessoas", "csv");

            var lines = Helper.CapturarLinhas(arquivo);

            foreach (var item in lines)
            {
                var v = item.Split(',');

                if (v.Length < 6)
                    while (v.Length < 6) v = v.Append("").ToArray();

                if (v.Length > 0)
                    Participantes.Add(new Participante(v[0], v[1], v[2], v[3], v[4], v[5]));
            }
        }

        public long TotalParticipantes()
        {
            return Participantes.Count(participante =>
                participante.Idade >= IDADEMINIMA &&
                participante.Renda >= RENDAMINIMA &&
                participante.Renda <= RENDAMAXIMA &&
                participante.Data.HasValue &&
                Helper.ValidarCpf(participante.Cpf) && (
                    (participante.Idade > 60 && string.IsNullOrEmpty(participante.Cid) && participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower()) || 
                    (!string.IsNullOrEmpty(participante.Cid) && participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower()) ||
                    participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower())
            );
        }

        public List<Participante> ListarParticipantesValidos()
        {
            return Participantes.FindAll(participante =>
                participante.Idade >= IDADEMINIMA &&
                participante.Renda >= RENDAMINIMA &&
                participante.Renda <= RENDAMAXIMA &&
                participante.Data.HasValue &&
                Helper.ValidarCpf(participante.Cpf)
            );
        }

        public List<Participante> ListarParticipantesAptos()
        {
            return Participantes.FindAll(participante =>
                participante.Idade >= IDADEMINIMA &&
                participante.Renda >= RENDAMINIMA &&
                participante.Renda <= RENDAMAXIMA &&
                participante.Data.HasValue &&
                Helper.ValidarCpf(participante.Cpf) && (
                    (participante.Idade > 60 && participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower()) ||
                    (!string.IsNullOrEmpty(participante.Cid) && participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower()) ||
                    participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower())
            );
        }
    }
}
