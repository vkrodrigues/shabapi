﻿using shab.models.Models;
using System.Collections.Generic;

namespace shab.repository.Interface
{
    public interface IParticipanteRepository
    {
        long TotalParticipantes();
        List<Participante> ListarParticipantesValidos();
        List<Participante> ListarParticipantesAptos();
    }
}
