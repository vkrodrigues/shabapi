﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace shab.utility
{
    public static class Helper
    {
        public static string DescricaoEnum(Enum value)
        {
            return
                value
                    .GetType()
                    .GetMember(value.ToString()).FirstOrDefault()
                    ?.GetCustomAttribute<DescriptionAttribute>()
                    ?.Description;
        }

        public static StreamReader LerArquivo(string caminho, string nomeArquivo, string extensao)
        {
            var path = Directory.GetCurrentDirectory();
            path = path.Replace("shab.api", caminho);

            return new StreamReader($@"{path}\\{nomeArquivo}.{extensao}");
        }

        public static List<string> CapturarLinhas(StreamReader arquivo, bool removerTitulo = true)
        {
            String line = arquivo.ReadToEnd();

            var lines = string.IsNullOrEmpty(line) ? new List<string>() : line.Replace("\r\n", "=").Split('=').ToList();

            if (removerTitulo)
                lines.RemoveAt(0);

            return lines;
        }

        public static bool ValidarCpf(string cpf)
        {
            if (string.IsNullOrEmpty(cpf) || string.IsNullOrEmpty(cpf.Trim()))
                return false;

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            cpf = cpf.Trim().Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;

            for (int j = 0; j < 10; j++)
                if (j.ToString().PadLeft(11, char.Parse(j.ToString())) == cpf)
                    return false;

            string tempCpf = cpf[..9];
            int soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            int resto = soma % 11;

            if (resto < 2) resto = 0;
            else resto = 11 - resto;

            string digito = resto.ToString();

            tempCpf += digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;

            if (resto < 2) resto = 0;
            else resto = 11 - resto;

            digito += resto.ToString();

            return cpf.EndsWith(digito);
        }

        public static DateTime? ConverterData(string data)
        {
            if (DateTime.TryParse(data, out DateTime dt))
                return dt;
            return null;
        }

        public static decimal ConverterValor(string valor)
        {
            if (decimal.TryParse(valor, out decimal result))
                return result;
            return 0;
        }

        public static string RemoverCaracteresEspeciais(string texto, bool space = false)
        {
            if (string.IsNullOrEmpty(texto))
                return texto;

            var retorno = texto.ToLower();

            retorno = Regex.Replace(retorno, @"([eÉÊË?èéêë?]+)", "e");
            retorno = Regex.Replace(retorno, @"([aÁÂÃÄÅÆàáâãäåæ]+)", "a");
            retorno = Regex.Replace(retorno, @"([iÍÎÏ?ìíîï?]+)", "i");
            retorno = Regex.Replace(retorno, @"([o?ÒÓÔÕÖØðòóôõöø]+)", "o");
            retorno = Regex.Replace(retorno, @"([uÙÚÛÜùúûü]+)", "u");
            retorno = Regex.Replace(retorno, @"([cçCÇ]+)", "c");

            retorno = Regex.Replace(retorno, @"([ºª°&%$#@!]+)", "");

            retorno = retorno
                .Replace("[", "")
                .Replace("]", "")
                .Replace(":", "")
                .Replace("*", "")
                .Replace("(", "")
                .Replace(")", "")
                .Replace(".", "")
                .Replace("?", "")
                .Replace("-", "")
                .Replace("|", "");

            if (space)
                retorno = retorno.Replace(" ", "");

            return retorno;
        }
    
        public static string EsconderCpf(string cpf)
        {
            cpf = Helper.RemoverCaracteresEspeciais(cpf, true);
            return $"{cpf[..3]}.XXX.XXX-X{cpf[^1..]}";
        }
    }
}
