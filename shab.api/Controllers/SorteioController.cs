﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using shab.business.Implementation;
using shab.business.Inerface;
using System;

namespace shab.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SorteioController : ControllerBase
    {
        private readonly ISorteioBusiness business;

        public SorteioController(ISorteioBusiness _business)
        {
            business = _business;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var ganhadores = business.RealizarSorteio();
                return Ok(ganhadores);
            }
            catch (Exception ex)
            {
                return StatusCode(ex.HResult, ex);
            }
        }
    }
}
