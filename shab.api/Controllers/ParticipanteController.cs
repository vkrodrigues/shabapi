﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using shab.business.Implementation;
using shab.business.Inerface;
using System;

namespace shab.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParticipanteController : ControllerBase
    {
        private readonly IParticipanteBusiness business;

        public ParticipanteController(IParticipanteBusiness _business)
        {
            business = _business;
        }

        [HttpGet("{tipo}")]
        public IActionResult ParticipantesPorTipo([FromRoute] string tipo)
        {
            try
            {
                var participantes = business.ListarParticipantesPorTipo(tipo);

                return Ok(participantes);
            }
            catch (Exception ex)
            {
                return StatusCode(ex.HResult, ex);
            }
        }

        [HttpGet("total")]
        public IActionResult TotalParticipantes()
        {
            try
            {
                var participantes = business.QtdParticipantes();

                return Ok(participantes);
            }
            catch (Exception ex)
            {
                return StatusCode(ex.HResult, ex);
            }
        }
    }
}
