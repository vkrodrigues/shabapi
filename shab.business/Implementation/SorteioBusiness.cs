﻿using shab.business.Inerface;
using shab.models.Enum;
using shab.models.Views;
using shab.utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace shab.business.Implementation
{
    public class SorteioBusiness : ISorteioBusiness
    {
        private readonly IParticipanteBusiness participanteBusiness;

        public SorteioBusiness(IParticipanteBusiness _participanteBusiness)
        {
            participanteBusiness = _participanteBusiness;
        }

        public List<ParticipanteView> RealizarSorteio()
        {
            var participantes = participanteBusiness.ListarParticipantesAptos();

            List<ParticipanteView> sorteados = new();

            var idososGanhadores = Sortear(participantes.Where(x => x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower()).ToList(), 1);
            var deficientesGanhadores = Sortear(participantes.Where(x => x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower()).ToList(), 1);
            var geralGanhadores = Sortear(participantes.Where(x => x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower()).ToList(), 3);

            sorteados.AddRange(idososGanhadores);
            sorteados.AddRange(deficientesGanhadores);
            sorteados.AddRange(geralGanhadores);

            return sorteados;
        }

        private static List<ParticipanteView> Sortear(List<ParticipanteView> participantes, int qtdSorteados)
        {
            List<ParticipanteView> sorteados = new();
            Random rnd = new();

            for (int i = 1; i <= qtdSorteados; i++)
            {
                var posicao = rnd.Next(0, participantes.Count - 1); 
                sorteados.Add(participantes[posicao]);
                participantes.RemoveAt(posicao);
            }

            return sorteados;
        }
    }
}
