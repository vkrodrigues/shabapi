﻿using shab.business.Inerface;
using shab.models.Enum;
using shab.models.Models;
using shab.models.Views;
using shab.repository.Implementation;
using shab.repository.Interface;
using shab.utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace shab.business.Implementation
{
    public class ParticipanteBusiness : IParticipanteBusiness
    {
        private readonly IParticipanteRepository participanteRepository;

        public ParticipanteBusiness(IParticipanteRepository _participanteRepository)
        {
            participanteRepository = _participanteRepository;
        }

        public long QtdParticipantes()
        {
            var total = participanteRepository.TotalParticipantes();

            return total;
        }

        public List<ParticipanteView> ListarParticipantesPorTipo(string tipo)
        {
            if (!Enum.TryParse(tipo, out ETipoCota tipoCota))
                throw new Exception("Tipo inválido");

            var participantes = participanteRepository.ListarParticipantesValidos();

            participantes = (tipoCota) switch 
            {
                ETipoCota.idoso => participantes.FindAll(x => x.Idade > 60 && string.IsNullOrEmpty(x.Cid) && x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower()),
                ETipoCota.deficiente => participantes.FindAll(x => !string.IsNullOrEmpty(x.Cid) && x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower()),
                ETipoCota.geral => participantes.FindAll(x => x.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower()),
                _ => new(),
            };

            return MontarView(participantes);
        }

        public List<ParticipanteView> ListarParticipantesAptos()
        {
            var participantes = participanteRepository.ListarParticipantesAptos();

            return MontarView(participantes);
        }

        private static List<ParticipanteView> MontarView(List<Participante> participantes)
        {
            return participantes.Select(x => new ParticipanteView(x.Nome, x.Cpf, x.Cota))
                .OrderBy(x => x.Nome)
                .ToList();
        }
    }
}
