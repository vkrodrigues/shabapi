﻿using shab.models.Views;
using System.Collections.Generic;

namespace shab.business.Inerface
{
    public interface ISorteioBusiness
    {
        List<ParticipanteView> RealizarSorteio();
    }
}
