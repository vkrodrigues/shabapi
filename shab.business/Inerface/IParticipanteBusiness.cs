﻿using shab.models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shab.business.Inerface
{
    public interface IParticipanteBusiness
    {
        long QtdParticipantes();
        List<ParticipanteView> ListarParticipantesPorTipo(string tipo);
        List<ParticipanteView> ListarParticipantesAptos();
    }
}
