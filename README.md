# Sistema de Sorteio Habitação - Api

Sistema utilizado para sortear pessoas que receberão o benefício e conquistar o sonho da casa própria. Ele se basea em um sistema de cota definido por Idosos, Deficiente físicos e Geral.

## 🚀 Começando

Essas instruções permitirão que você obtenha uma cópia do projeto em operação na sua máquina local para fins de desenvolvimento e teste.

### 📋 Ferramentas Utilizadas

* [C#](https://docs.microsoft.com/pt-br/dotnet/csharp/) - Linguagem orientada a objetos da Microsoft
* [NET core 5](https://dotnet.microsoft.com/en-us/download/dotnet/5.0) - Framework utilizado
* [MSTest](https://docs.microsoft.com/en-us/dotnet/api/microsoft.visualstudio.testtools.unittesting?view=visualstudiosdk-2022) - Biblioteca utilizada para teste

### 🛠️ Configurando o Projeto

Utilize o caminho abaixo para clonar o projeto:

```
https://gitlab.com/vkrodrigues/shabapi.git
```

Iniciando o projeto:

```
Inicie o projeto a partir do shab.api
```

## ⚙️ Executando os testes

Para executar os testes é necessário executar o projeto shab.test através do menu ou do comando CTRL+R,A.

### 🔩 Analise os testes de ponta a ponta

* ParticipanteTeste - Responsável por realizar todos os testes sobre os participantes e garantir a validação.
* SorteioTeste - Responsável por garantir a quantidade de pessoas por cota durante o sorteio.

## ✒️ Autores

* **Um desenvolvedor** - *Trabalho Inicial* - [vkrodrigues](https://github.com/Viktoradr)

---
