﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using shab.business.Implementation;
using shab.business.Inerface;
using shab.models.Enum;
using shab.models.Models;
using shab.repository.Implementation;
using shab.repository.Interface;
using shab.utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shab.test
{
    [TestClass]
    public class ParticipanteTeste
    {
        private readonly IParticipanteRepository repository;

        public ParticipanteTeste()
        {
            repository = new ParticipanteRepository();
        }

        [TestMethod]
        public void ValidandoTotalAptos()
        {
            List<Participante> participantes = repository.ListarParticipantesAptos();

            long total = repository.TotalParticipantes();

            Assert.Equals(participantes.Count, total);
        }

        [TestMethod]
        public void ValidandoIdososAptos()
        {
            List<Participante> participantesIdoso = repository.ListarParticipantesAptos();

            bool idadeValida = participantesIdoso.All(participante => participante.Idade > 60);
            bool cotaValido = participantesIdoso.All(participante => participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower());
            bool cidValidado = participantesIdoso.All(participante => string.IsNullOrEmpty(participante.Cid));

            Assert.IsTrue(cidValidado && idadeValida && cotaValido);

        }

        [TestMethod]
        public void ValidandoDeficienteAptos()
        {
            List<Participante> participantesIdoso = repository.ListarParticipantesAptos();

            bool cotaValido = participantesIdoso.All(participante => participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower());
            bool cidValidado = participantesIdoso.All(participante => !string.IsNullOrEmpty(participante.Cid));

            Assert.IsTrue(cidValidado && cotaValido);
        }

        [TestMethod]
        public void ValidandoGeralAptos()
        {
            List<Participante> participantesIdoso = repository.ListarParticipantesAptos();

            bool cotaValido = participantesIdoso.All(participante => participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower());

            Assert.IsTrue(cotaValido);
        }
    }
}
