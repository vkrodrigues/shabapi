using Microsoft.VisualStudio.TestTools.UnitTesting;
using shab.business.Implementation;
using shab.business.Inerface;
using shab.models.Enum;
using shab.models.Views;
using shab.repository.Implementation;
using shab.utility;
using System.Collections.Generic;
using System.Linq;

namespace shab.test
{
    [TestClass]
    public class SorteioTeste
    {
        private readonly ISorteioBusiness business;

        public SorteioTeste()
        {
            business = new SorteioBusiness(new ParticipanteBusiness(new ParticipanteRepository()));
        }

        [TestMethod]
        public void VencedoresEsperadosParaIdoso()
        {
            List<ParticipanteView> participantes = business.RealizarSorteio();
            int qtdVencedores = 1;

            List<ParticipanteView> sorteados = participantes.Where(participante => 
                participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.idoso).ToLower()).ToList();

            Assert.Equals(sorteados.Count, qtdVencedores);
        }

        [TestMethod]
        public void VencedoresEsperadosParaDeficiente()
        {
            List<ParticipanteView> participantes = business.RealizarSorteio();
            int qtdVencedores = 1;

            List<ParticipanteView> sorteados = participantes.Where(participante =>
                participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.deficiente).ToLower()).ToList();

            Assert.Equals(sorteados.Count, qtdVencedores);
        }

        [TestMethod]
        public void VencedoresEsperadosParaGeral()
        {
            List<ParticipanteView> participantes = business.RealizarSorteio();
            int qtdVencedores = 3;

            List<ParticipanteView> sorteados = participantes.Where(participante =>
                participante.Cota.ToLower() == Helper.DescricaoEnum(ETipoCota.geral).ToLower()).ToList();

            Assert.Equals(sorteados.Count, qtdVencedores);
        }

        [TestMethod]
        public void VencedoresEsperadosTodos()
        {
            List<ParticipanteView> participantes = business.RealizarSorteio();
            int qtdVencedores = 5;

            List<ParticipanteView> sorteados = new();
            participantes.ForEach(participante => sorteados.Add(participante));

            Assert.Equals(sorteados.Count, qtdVencedores);
        }

    }
}
