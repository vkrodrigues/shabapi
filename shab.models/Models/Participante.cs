﻿using shab.models.Enum;
using shab.utility;
using System;
using System.Collections.Generic;

namespace shab.models.Models
{
    public class Participante
    {
        public Participante() { }

        public Participante(string nome, string cpf, string data, string renda, string cota, string cid) 
        {
            Nome = nome?.Trim();
            Cpf = cpf?.Trim();
            Data = Helper.ConverterData(data);
            Renda = Helper.ConverterValor(renda);
            Cota = (new List<string> { "idoso", "deificente físico", "geral" }).Contains(cota.ToLower()) ? cota.ToUpper() : string.Empty;
            Cid = cid?.Trim();
            Idade = Data.HasValue ? (int)((DateTime.Today.Subtract(Data.Value)).Days / 365.25) : 0;
        }

        public string Nome { get; set; }
        public string Cpf { get; set; }
        public DateTime? Data { get; set; }
        public decimal Renda { get; set; }
        public string Cota { get; set; }
        public string Cid { get; set; }
        public int Idade { get; set; }
    }
}
