﻿using System.ComponentModel;

namespace shab.models.Enum
{
    public enum ETipoCota
    {
        [Description("idoso")]
        idoso,

        [Description("deificente físico")]
        deficiente,

        [Description("geral")]
        geral
    }
}
