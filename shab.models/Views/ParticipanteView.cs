﻿using shab.utility;
using System;

namespace shab.models.Views
{
    public class ParticipanteView
    {
        public ParticipanteView(string nome, string cpf, string cota)
        {
            Nome = nome;
            Cpf = MascararCpf(cpf);
            Cota = cota.ToUpper();
        }

        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Cota { get; set; }

        private static string MascararCpf(string cpf) => Helper.EsconderCpf(cpf);
    }
}
